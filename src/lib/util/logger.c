/**
 * @file
 *****************************************************************************
 * @title   logger.c
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   Logging implementation to common logger task
 *******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stddef.h>
#include <stdbool.h>
#include <string.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "conversions.h"
#include "printf.h"
#include "os.h"
#include "logger.h"

/* Implementation -------------------------------------------------------------*/

#define LOG_MSG_SIZE            80
#define LOG_MSG_QUEUE_ENTRIES   2

static xQueueHandle log_queue = NULL;
static xTaskHandle log_task;
static bool log_task_is_enabled = true;

typedef struct
{
    char buf[LOG_MSG_SIZE];
} logmsg_entry_t;


/**
 * function is used to place characters into buffer
 */
static void putf(void* p P_UNUSED, char ch, sprintf_state_t* state)
{
    // write characters into buffer until max. bytes reached or \n encountered
    logmsg_entry_t* entry = state->ctx;
    if (state->buf_left > 0)
    {
        entry->buf[LOG_MSG_SIZE - state->buf_left] = ch;
        state->buf_left--;
    }
}

extern uint16_t VCP_DataTx(uint8_t* Buf, uint32_t Len);

/**
 * Sends message to logging task
 */
static void send_msg(const sprintf_state_t* state)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    logmsg_entry_t* entry = state->ctx;

    if (log_task_is_enabled)
    {
        // send away to logging task
        xQueueSendFromISR(log_queue, entry->buf, &xHigherPriorityTaskWoken);
    }
    else
    {
        entry->buf[LOG_MSG_SIZE-1] = '\0';
        (void) VCP_DataTx(entry->buf, LOG_MSG_SIZE-state->buf_left);
    }

    if (xHigherPriorityTaskWoken)
    {
        taskYIELD();
    }
}

/**
 * Logging task reads from message queue and prints out to stdout
 */
static void log_msg_task(void *arg)
{
    xQueueHandle queue = (xQueueHandle) arg;
    char buf[LOG_MSG_SIZE + 1];

    do
    {
        if (log_task_is_enabled)
        {
            if (pdTRUE == xQueueReceive(queue, buf, 100000))
            {
                buf[LOG_MSG_SIZE] = '\0';
                // XXX DS: use either printf for USART or snprintf in buffer for USB
                printf(buf);
            }
        }
        else
        {
            // wait until logger is enabled again
            vTaskDelay(1);
        }
    } while (1);
}


/**
 * Enables or disabled logger task. If it is enabled, the logger task reads from
 * internal queue the next msg. to print to usart. If disabled the logger task
 * will do nothing.
 *
 * @param enable    if true, logger task is enabled. If false logger task is disabled
 */
void log_task_enable(bool enable)
{
    log_task_is_enabled = enable;
}


/**
 * Returns if logger task is enabled.
 *
 * @return  true if logger task is enabled, false otherwise
 */
bool log_is_task_enabled(void)
{
    return log_task_is_enabled;
}

/**
 * Initializes the logging facility. Starts a task with priority 3 that waits
 * for logging messages sent from other tasks or from ISR context.
 * The max. possible queue size of the task is 3
 *
 * @return      0 means OK, -1 means insufficient memory for allocating related
 *              resources or logging utility already initialized
 *
 * @note    Must not be called from within ISR
 */
bool log_init()
{
    // Create Message queue
    log_queue = xQueueCreate(LOG_MSG_QUEUE_ENTRIES, LOG_MSG_SIZE);
    if (log_queue == 0)
    {
        return false;
    }

    // Spawn logger task
    portBASE_TYPE rv = xTaskCreate(log_msg_task,
            (const char * const) "Logger",
            104,
            log_queue,
            5,
            &log_task);
    if (rv != pdPASS)
    {
        return false;
    }

    os_add_tcb(log_task);

    // assign putf function ?
    // Mark as initialized

    return true;
}

/**
 * Sends message to logging task. Formats given string in printf compatible way.
 *
 * The max. size of the output buffer is determined via log_msg_max().
 *
 * @param   fmt     printf style compatible format.
 *
 * @return  number of bytes send to logging task. Can be smaller than actual size of
 *          the formatted string if it would be bigger than the size returned via
 *          log_msg_max().
 *          In case of an error -1 is returned. This means the logging task has not
 *          yet been created via log_init().
 *
 * @note The following printf format characters are supported:
 *       's d u i'
 * @note This function may be called from inside an ISR as it does non blocking I/O
 *       to a message queue.
 */
int log_msg(const char* fmt, ...)
{
    va_list va;
    logmsg_entry_t entry;
    memset(&entry, 0, sizeof(entry));
    sprintf_state_t state =
    { .buf_left = log_msg_max(), .ctx = &entry };
    va_start(va, fmt);
    tfp_format(NULL, putf, &state, fmt, va);
    va_end(va);
    int pos = log_msg_max() - state.buf_left;
    if (state.buf_left >= 2)
    {
        /* append newline */
        entry.buf[pos] = '\r';
        entry.buf[pos+1] = '\n';
        pos += 2;
        state.buf_left -= 2;
    }
    send_msg(&state);
    return (pos);
}

/**
 * Returns number of max. possible message size that a message sent to the
 * logging task can hold.
 * Any message bigger than the size returned will be cut.
 *
 */
size_t log_msg_max()
{
    return LOG_MSG_SIZE;
}

/* EOF */

